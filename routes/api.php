<?php

/*
 * Separated simple api & api with sessions
 * See app/Http/Kernel.php, line 45
 */

Route::middleware('api')->group(static function () {});

Route::middleware('apiWithSession')->group(static function () {
    Route::post('raffle/play', 'RaffleController@play')->name('raffle.play');
});

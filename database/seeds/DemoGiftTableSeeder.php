<?php

use App\Models\Gift;
use Illuminate\Database\Seeder;

class DemoGiftTableSeeder extends Seeder
{
    public const AMOUNT_GIFTS_GENERATING = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Gift::class, self::AMOUNT_GIFTS_GENERATING)->create();
    }
}

<?php
declare(strict_types=1);

use Illuminate\Database\Seeder;
use App\Models\Storage;

class StorageTableSeeder extends Seeder
{
    public const STORAGE_ITEMS = [
        Storage::KEY_MONEY_AMOUNT => 0,
    ];

    /**
     * @throws Exception
     */
    public function run(): void
    {
        DB::beginTransaction();

        foreach (self::STORAGE_ITEMS as $key => $value) {
            $this->findOrCreate($key, $value);
        }

        DB::commit();
    }

    protected function findOrCreate(string $key, $value): void
    {
        $exists = Storage::query()
            ->where('key', $key)
            ->exists();

        if (!$exists) {
            Storage::query()->create([
                'key' => $key,
                'value' => $value,
            ]);
        }
    }
}

<?php
declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Gift;
use Faker\Generator as Faker;

$factory->define(Gift::class, static function (Faker $faker) {
    return [
        'name' => $faker->word,
        'factor' => $faker->randomFloat(2, 1, 100),
        'amount' => $faker->numberBetween(5, 10),
    ];
});

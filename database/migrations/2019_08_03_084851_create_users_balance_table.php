<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        DB::beginTransaction();

        Schema::create('users_balance', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->decimal('money', 15, 2)->default(0);
            $table->bigInteger('points')->default(0);
        });

        Schema::table('users_balance', static function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        DB::beginTransaction();

        Schema::table('users_balance', static function (Blueprint $table) {
            $table->dropForeign('user_balance_user_id_foreign');
        });

        Schema::dropIfExists('users_balance');

        DB::commit();
    }
}

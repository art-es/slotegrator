<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGiftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        DB::beginTransaction();

        Schema::create('user_gift', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('gift_id')->unsigned();
        });

        Schema::table('user_gift', static function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('gift_id')
                ->references('id')->on('gifts')
                ->onDelete('cascade');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();

        Schema::table('user_gift', static function (Blueprint $table) {
            $table->dropForeign('user_gift_user_id_foreign');
        });

        Schema::dropIfExists('user_gift');

        DB::commit();
    }
}

<?php
declare(strict_types=1);

namespace Tests\Unit;

use App\Models\User;
use App\Services\ConvertingPrizesService;
use App\Services\RaffleService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ConvertingPrizesTest extends TestCase
{
    use RefreshDatabase,
        WithFaker;

    /** @var User $user */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->user->balance()->create();
    }

    public function testConvertingMoneyToPoints(): void
    {
        $this->be($this->user);

        $fakeMoney = (float) $this->faker->randomFloat(2, RaffleService::MIN_MONEY, RaffleService::MAX_MONEY);

        $balance = $this->user->balance;
        $balance->money = $fakeMoney;
        $balance->points = 0;
        $balance->save();

        $convertedMoney = (new ConvertingPrizesService($this->user))
            ->moneyToPoints($balance->money);

        $this->assertEquals($fakeMoney, $convertedMoney);
        $this->assertDatabaseHas('users_balance', [
            'user_id' => $this->user->id,
            'money' => 0.0,
            'points' => floor($fakeMoney * ConvertingPrizesService::MONEY_FACTOR),
        ]);
    }
}

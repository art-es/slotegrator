<?php
declare(strict_types=1);

namespace App\Jobs;

use App\Models\Storage;
use App\Models\User;
use App\Models\UserBalance;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMoneyToBankAccount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected
        /** @var User $user */
        $user,
        /** @var UserBalance $balance */
        $balance,
        /** @var float $perPack */
        $perPack;

    /**
     * Create a new job instance.
     *
     * @param  UserBalance  $balance
     * @param $perPack
     */
    public function __construct(UserBalance $balance, $perPack)
    {
        $this->balance = $balance;
        $this->user = $balance->user;
        $this->perPack = $perPack;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        DB::beginTransaction();

        // Chunks sending
        $chunks = ceil($this->balance->money / $this->perPack);

        // Money in storage
        $moneyAmount = Storage::moneyAmount()->value ?? 0;

        for ($i = 0; $i < $chunks; $i++) {
            if ($moneyAmount <= 0) {
                $this->setMoneyAmountOrCreate(0.0);
                return;
            }

            $money = $this->perPack;

            if ($money > $this->balance->money) {
                $money = $this->balance->money;
            }

            // Sending to bank account by API
            // Count: $money

            $this->balance->money -= $money;
        }

        $this->balance->save();
        $this->setMoneyAmountOrCreate((float) $moneyAmount);

        DB::commit();
    }

    protected function setMoneyAmountOrCreate(float $money): void
    {
        $moneyAmount = Storage::moneyAmount();

        If ($moneyAmount === null) {
            Storage::createMoneyAmount();
            return;
        }

        $moneyAmount->value = $money;
        $moneyAmount->save();
    }
}

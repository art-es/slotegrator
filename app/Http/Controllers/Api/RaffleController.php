<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\RaffleService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class RaffleController extends Controller
{
    private $raffle;

    public function __construct(RaffleService $raffle)
    {
        $this->raffle = $raffle;
    }

    public function play(): JsonResponse
    {
        $prize = $this->raffle->play();
        $prize->giveTo(Auth::user());

        return app('response.json')->with([
            'type' => $prize->type,
            'value' => $prize->value,
        ]);
    }
}

<?php
declare(strict_types=1);

namespace App\Foundation;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 * @package App\Foundation
 */
class Repository
{
    /** @var Model $model */
    protected $model;

    /** @return Model */
    public function getModel(): Model
    {
        return $this->model;
    }
}

<?php
declare(strict_types=1);

namespace App\Foundation;

use App\Models\Gift;
use App\Models\User;
use App\Services\GivePrizeService;

/**
 * Class Prize
 * @package App\Foundation
 * @property string|null $type
 * @property float|int|Gift|null $value
 */
class Prize
{
    /**
     * Type of prize.
     * Variants:
     *      'money', 'points', 'gift'
     *
     * @var string|null $type
     */
    public $type;

    /**
     * Value of prize.
     *
     * @var float|int|Gift|null
     */
    public $value;

    public function giveTo(User $user): void
    {
        (new GivePrizeService($this, $user))->handle();
    }
}

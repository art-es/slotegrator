<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Jobs\SendMoneyToBankAccount;
use App\Models\Storage;
use App\Models\UserBalance;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class SendMoney extends Command
{
    protected
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        $signature = 'send:money {per_pack}',
        /**
     * The console command description.
     *
     * @var string
     */
        $description = 'Send money by N per pack',
        /**
         * Sending by n per pack to bank account of users.
         *
         * @var float $perPack
         */
        $perPack;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        try {
            $this->setPerPack();
            $this->dispatchingBalances();
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    protected function dispatchingBalances(): void
    {
        $moneyAmount = Storage::moneyAmount()->value ?? 0;

        /** @var UserBalance $balance */
        foreach ($this->getBalances() as $balance) {
            if ($moneyAmount <= 0) {
                return;
            }

            dispatch(new SendMoneyToBankAccount($balance, $this->perPack));
            $moneyAmount -= $balance->money;
        }
    }

    protected function getBalances(): Collection
    {
        return UserBalance::query()
            ->where('money', '>', 0)
            ->get();
    }

    protected function setPerPack(): void
    {
        $this->perPack = (float) $this->argument('per_pack');
    }
}

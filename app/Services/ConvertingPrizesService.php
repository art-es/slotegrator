<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\User;

class ConvertingPrizesService
{
    public const MONEY_FACTOR = 2.5;

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Converting money to points.
     *
     * @param  float  $moneyPack  Pack of converting money.
     * @return float Amount of converted money.
     */
    public function moneyToPoints(float $moneyPack): float
    {
        $balance = $this->user->balance;

        if ($balance === null) {
            $this->user->balance()->create();

            // balance just created
            return 0.0;
        }

        $money = $moneyPack;

        if ($balance->money < $moneyPack) {
            $money = $balance->money;
        }

        $balance->points += floor($money * self::MONEY_FACTOR);
        $balance->money -= $money;
        $balance->save();

        return $money;
    }
}

<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use function is_array;
use function is_object;

/**
 * Class JsonResponseService
 * Service for getting formatted JSON response.
 * @package App\Services
 */
class JsonResponseService extends JsonResponse
{
    /**
     * Set success field to false.
     *
     * @return self
     */
    public function fail(): self
    {
        $this->setStatusCode(self::HTTP_BAD_REQUEST);

        $currentData = $this->getDecodedData();
        $currentData['success'] = false;

        $this->setData($currentData);

        return $this;
    }

    /**
     * Get only your data to response.
     *
     * @param  mixed  $data
     * @return self
     */
    public function only($data): self
    {
        $this->setData($data);

        return $this;
    }

    /**
     * Set success field to true.
     *
     * @return self
     */
    public function success(): self
    {
        $this->setStatusCode(self::HTTP_OK);

        $currentData = $this->getDecodedData();
        $currentData['success'] = true;

        $this->setData($currentData);

        return $this;
    }

    /**
     * Throw http response exception.
     *
     * @throws HttpResponseException
     */
    public function throw(): void
    {
        $this->setStatusCode(self::HTTP_BAD_REQUEST);
        throw new HttpResponseException($this);
    }

    /**
     * Get default response data with your data.
     *
     * @param  mixed  $data
     * @return self
     */
    public function with($data): self
    {
        $currentData = $this->getDecodedData();

        if (is_array($data) || is_object($data)) {
            $data = array_merge($currentData, (array) $data);
        } else {
            $currentData[] = $data;
            $data = $currentData;
        }

        $this->setData($data);

        return $this;
    }

    /**
     * Get decoded current data.
     *
     * @return mixed
     */
    protected function getDecodedData()
    {
        return json_decode($this->data, true);
    }
}

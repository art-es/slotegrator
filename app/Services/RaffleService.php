<?php
declare(strict_types=1);

namespace App\Services;

use App\Foundation\Prize;
use App\Models\Gift;
use App\Models\User;
use Faker\Factory;

class RaffleService
{
    public const
        PRIZE_TYPE_MONEY = 'money',
        PRIZE_TYPE_POINTS = 'points',
        PRIZE_TYPE_GIFT = 'gift';

    public const
        MIN_MONEY = 1,
        MAX_MONEY = 100,
        MIN_POINTS = 1,
        MAX_POINTS = 100;

    protected
        /**
         * User with whose balance are working.
         *
         * @var User|null $user
         */
        $user,
        /**
         * Prize of raffle.
         *
         * @var Prize $prize
         */
        $prize;

    public function __construct(?User $user)
    {
        $this->user = $user;
        $this->prize = new Prize;
    }

    public function play(): Prize
    {
        $this->randomPrize();
        return $this->prize;
    }

    protected function getRandomPrizeType(): string
    {
        $type = collect([
            self::PRIZE_TYPE_MONEY,
            self::PRIZE_TYPE_POINTS,
            self::PRIZE_TYPE_GIFT,
        ])->random();

        return (string) $type;
    }

    protected function randomGift(): void
    {
        $gift = Gift::query()
            ->inRandomOrder()
            ->where('amount', '>', 0)
            ->first();

        if ($gift === null) {
            return;
        }

        $this->prize->type = self::PRIZE_TYPE_GIFT;
        $this->prize->value = $gift;
    }

    protected function randomMoney(): void
    {
        $this->prize->type = self::PRIZE_TYPE_MONEY;
        $this->prize->value = (float) Factory::create()->randomFloat(2, self::MIN_MONEY, self::MAX_MONEY);
    }

    protected function randomPoints(): void
    {
        $this->prize->type = self::PRIZE_TYPE_POINTS;
        $this->prize->value = Factory::create()->numberBetween(self::MIN_POINTS, self::MAX_POINTS);
    }

    protected function randomPrize(): void
    {
        $prizeType = $this->getRandomPrizeType();

        switch ($prizeType) {
            case self::PRIZE_TYPE_MONEY:
                $this->randomMoney();
                return;

            case self::PRIZE_TYPE_GIFT:
                $this->randomGift();
                if ($this->prize->type !== null) {
                    return;
                }
        }

        $this->randomPoints();
    }
}

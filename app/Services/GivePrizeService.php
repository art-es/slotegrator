<?php
declare(strict_types=1);

namespace App\Services;

use App\Foundation\Prize;
use App\Models\Gift;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use RuntimeException;

class GivePrizeService
{
    protected
        /**
         * Prize which is issued.
         *
         * @var Prize $prize
         */
        $prize,
        /**
         * User whose give a prize.
         *
         * @var User $user
         */
        $user;

    public function __construct(Prize $prize, User $user)
    {
        $this->prize = $prize;
        $this->user = $user;
    }

    public function handle(): void
    {
        try {
            switch ($this->prize->type) {
                case RaffleService::PRIZE_TYPE_MONEY:
                    $this->giveMoney();
                    return;

                case RaffleService::PRIZE_TYPE_GIFT:
                    $this->giveGift();
                    return;

                case RaffleService::PRIZE_TYPE_POINTS:
                    $this->givePoints();
                    return;
            }
        } catch (Exception $exception) {
            app('response.json')
                ->fail()
                ->with(['error' => $exception->getMessage()])
                ->throw();
        }
    }

    /**
     * @throws Exception
     */
    protected function giveGift(): void
    {
        /** @var Gift $gift */
        $gift = $this->prize->value;

        if ($gift === null) {
            throw new RuntimeException('gift not found');
        }

        DB::beginTransaction();

        $this->user->gifts()->attach($gift->id);

        $gift->amount--;
        $gift->save();

        DB::commit();
    }

    protected function giveMoney(): void
    {
        $balance = $this->user->balance;

        if ($balance === null) {
            $balance = $this->user->balance()->create();
        }

        $balance->money += $this->prize->value;
        $balance->save();
    }

    protected function givePoints(): void
    {
        $balance = $this->user->balance;

        if ($balance === null) {
            $balance = $this->user->balance()->create();
        }

        $balance->points += $this->prize->value;
        $balance->save();
    }
}

<?php
declare(strict_types=1);

namespace App\Builders;

use App\Models\Storage;
use Illuminate\Database\Eloquent\Builder;

class StorageBuilder extends Builder
{
    /**
     * Add money amount row in storage.
     *
     * @return Storage
     */
    public function createMoneyAmount(): Storage
    {
        /** @var Storage $moneyAmount */
        $moneyAmount = $this->create([
            'key' => Storage::KEY_MONEY_AMOUNT,
            'value' => 0,
        ]);

        return $moneyAmount;
    }

    /**
     * Get money amount object.
     *
     * @return Storage|null
     */
    public function moneyAmount(): ?Storage
    {
        return $this
            ->where('key', Storage::KEY_MONEY_AMOUNT)
            ->first();
    }
}

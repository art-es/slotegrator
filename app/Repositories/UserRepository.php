<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserRepository
 * @package App\Repositories
 * @property User $model
 */
class UserRepository
{
    /**
     * UserRepository constructor.
     * @param  User  $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Create user, make hash from password, create user balance.
     *
     * @param  array  $data
     * @return User
     * @throws Exception
     */
    public function create(array $data): User
    {
        DB::beginTransaction();

        $this->model = $this->model::query()->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $this->model->balance()->create();

        DB::commit();

        return $this->model;
    }
}

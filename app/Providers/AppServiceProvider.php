<?php

namespace App\Providers;

use App\Services\JsonResponseService;
use App\Services\RaffleService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('response.json', static function () {
            return (new JsonResponseService())->success();
        });

        $this->app->bind('raffle', static function () {
            return new RaffleService(Auth::user());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

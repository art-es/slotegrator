<?php
declare(strict_types=1);

namespace App\Models;

use Artes\LaravelBuilder\Base\Builderable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Storage
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Storage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Storage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Storage query()
 * @method static self createMoneyAmount
 * @method static self moneyAmount
 * @mixin \Eloquent
 * @property int $id
 * @property string $key
 * @property mixed $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Storage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Storage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Storage whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Storage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Storage whereValue($value)
 */
class Storage extends Model
{
    use Builderable;

    public const KEY_MONEY_AMOUNT = 'money_amount';

    protected $table = 'storage';

    protected $fillable = [
        'key', 'value',
    ];
}

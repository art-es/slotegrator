<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\UserBalance
 *
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property float $money
 * @property int $points
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereUserId($value)
 */
class UserBalance extends Model
{
    protected $table = 'users_balance';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    public $timestamps = false;

    /*
     * Relationship
     */

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}

<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Gift
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property float $factor
 * @property int $amount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereFactor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gift whereName($value)
 */
class Gift extends Model
{
    protected $fillable = [
        'name', 'factor', 'amount'
    ];

    public $timestamps = false;

    /*
     * Relationship
     */

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_gift');
    }
}

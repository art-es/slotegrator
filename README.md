# Slotegrator test task

## Installation

Copy .env file
```
cp .env.example .env
```

Fill .env file:  
`DB_*`

Run commands:
```
composer install
php artisan key:generate
php artisan migrate --seed
npm i
npm run prod
```

If you want generate gifts
```
php artisan db:seed --class=DemoGiftTableSeeder
```

Command for sending money to bank accounts
```
php artisan send:money {per_pack}
```

Run tests  
Previous fill in .env.testing `DB_*` 
```
phpunit
```

Telegram: iamartes  
Github: art-es
